﻿using System;
using System.Linq;
using System.Reflection;
using AutoReferencer;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AutoReferencer
{
#if (UNITY_EDITOR)
    public class AutorefHelper
    {
        public static void SetReferences(Type t, MonoBehaviour refObject)
        {
            AutoRefAttribute classAttr = t.GetCustomAttribute<AutoRefAttribute>();
            if (classAttr == null)
            {
                Debug.LogError("Class " + t + " doesnt contain [AutoRef] Attribute");
                return;
            }

            t.GetFields().ToList().ForEach(field =>  HandleRefType(field, refObject, classAttr));
        }

        private static void HandleRefType(FieldInfo field, MonoBehaviour refObject, AutoRefAttribute classAttr)
        {
            AutoRefAttribute fieldAttr = field.GetCustomAttribute<AutoRefAttribute>();
            string fieldName = GetRefName(fieldAttr,field);

            BaseRefHandler childRefHandler = new ChildRefHandler(field, refObject,fieldName);
            BaseRefHandler parentRefHandler = new ParentRefHandler(field, refObject);
            BaseRefHandler selfRefHandler = new SelfRefHandler(field, refObject);
            BaseRefHandler noneRefHandler = new NoneRefHandler(field, refObject);
            BaseRefHandler sceneRefHandler = new SceneRefHandler(field,refObject,fieldName);

            childRefHandler.Successor = selfRefHandler;
            selfRefHandler.Successor = parentRefHandler;
            parentRefHandler.Successor = sceneRefHandler;
            sceneRefHandler.Successor = noneRefHandler;
           
            if (fieldAttr == null)
                childRefHandler.Handle(classAttr.refType);
            else
            {
                childRefHandler.Handle(fieldAttr.refType != AutoRefType.ChildName ? fieldAttr.refType : classAttr.refType);
            }

            EditorUtility.SetDirty(refObject);
            childRefHandler.Handle(fieldAttr?.refType ?? classAttr.refType);
        }

        private static string GetRefName( AutoRefAttribute fieldAttr,FieldInfo field) {
            if (fieldAttr == null)
            {
                return field.Name;
            }

            return fieldAttr.name ?? field.Name;
        }
    }
    #endif
}
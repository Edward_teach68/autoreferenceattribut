﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace AutoReferencer
{
    public abstract class BaseRefHandler
    {
        public FieldInfo field;
        public MonoBehaviour refObject;

        public BaseRefHandler Successor { get; set; }

        protected BaseRefHandler(FieldInfo field, MonoBehaviour refObject)
        {
            this.field = field;
            this.refObject = refObject;
        }
        
        public abstract void Handle(AutoRefType autoRefType);

    }
}

﻿using System;
using System.Reflection;
using UnityEngine;

namespace AutoReferencer
{
    public class ParentRefHandler : BaseRefHandler
    {
        
        public ParentRefHandler(FieldInfo field, MonoBehaviour refObject) : base(field, refObject)
        {
        }

        public override void Handle(AutoRefType autoRefType)
        {
            if (autoRefType == AutoRefType.Parent)
            {
                try
                {
                    var component = refObject.GetComponentInParent(field.FieldType);
                    if (component != null)
                        field.SetValue(refObject, component);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            else
                Successor?.Handle(autoRefType);
        }

  
    }
}

﻿using System;
using System.Reflection;
using UnityEngine;

namespace AutoReferencer
{
    public class SelfRefHandler : BaseRefHandler
    {
        public SelfRefHandler(FieldInfo field, MonoBehaviour refObject) : base(field, refObject)
        {
        }

        public override void Handle(AutoRefType autoRefType)
        {
            if (autoRefType == AutoRefType.Self)
            {
                try
                {
                    var component = refObject.GetComponent(field.FieldType);
                    if (component != null)
                        field.SetValue(refObject, component);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            else
                Successor?.Handle(autoRefType);
        }
    }
}
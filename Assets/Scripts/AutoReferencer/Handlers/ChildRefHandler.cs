﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace AutoReferencer
{
    public class ChildRefHandler : BaseRefHandler
    {
        public string fieldName;
        public ChildRefHandler(FieldInfo field, MonoBehaviour refObject, string fieldName) : base(field, refObject)
        {
            this.fieldName = fieldName;
        }

        public override void Handle(AutoRefType autoRefType)
        {
            if (autoRefType == AutoRefType.Child)
            {
                try
                {
                    List<Component> component = refObject.GetComponentsInChildren(field.FieldType,true)
                        .Where(c => refObject.gameObject != c.gameObject)
                        .ToList();
//                    Debug.Log(component.Count + " " + field.FieldType);
                    if (component.Count > 1)
                    {
                        field.SetValue(refObject, component.First(c => c.name == fieldName));
                        return;
                    }

                    if (component.Count == 1)
                        field.SetValue(refObject, component[0]);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            else
                Successor?.Handle(autoRefType);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace AutoReferencer
{
    public class NoneRefHandler : BaseRefHandler
    {
        public NoneRefHandler(FieldInfo field, MonoBehaviour refObject) : base(field, refObject)
        {
        }

        public override void Handle(AutoRefType autoRefType)
        {
            if (autoRefType == AutoRefType.None)
            {
            }
            else
                Successor?.Handle(autoRefType);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoReferencer;
using UnityEngine;
using Object = UnityEngine.Object;

public class SceneRefHandler : BaseRefHandler
{
    public string fieldName;
   
    public SceneRefHandler(FieldInfo field, MonoBehaviour refObject,string fieldName) : base(field, refObject)
    {
        this.fieldName = fieldName;
    }
    
    public override void Handle(AutoRefType autoRefType)
    {
        if (autoRefType == AutoRefType.Scene)
        {
            try
            {
                List<Object> component = Object.FindObjectsOfType(field.FieldType)
                    .ToList();
                if (component.Count > 1)
                {
                    field.SetValue(refObject, component.First(c => c.name == fieldName));
                    return;
                }

                if (component.Count == 1)
                    field.SetValue(refObject, component[0]);
            }
            catch (Exception e)
            {
                Debug.Log(e );
            }
        }
        else
            Successor?.Handle(autoRefType);
    }
}

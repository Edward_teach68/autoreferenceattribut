﻿using UnityEditor;
using UnityEngine;

#if (UNITY_EDITOR)
namespace AutoReferencer
{
    [CustomPropertyDrawer(typeof(AutoRefButtonAttribute))]
    public class AutoRefButtonDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 27.0f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            float width = position.width;
            position.height = 25.0f;
            position.width = 250.0f;
            position.x = (width - 250.0f) / 2;
            if (GUI.Button(position, "Set References"))
            {
                MonoBehaviour ob = property.serializedObject.targetObject as MonoBehaviour;
                if (ob == null) return;
                AutorefHelper.SetReferences(property.serializedObject.targetObject.GetType(), ob);
            }
        }
    }
}
#endif
﻿using System;

namespace AutoReferencer
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field)]
    public sealed class AutoRefAttribute : Attribute
    {
        public AutoRefAttribute()
        {
            refType = AutoRefType.Child;
        }
        
        public AutoRefAttribute(AutoRefType refType)
        {
            this.refType = refType;
        }
             
        public AutoRefAttribute(string name)
        {
            this.name = name;
            this.refType = AutoRefType.ChildName;
        }
        
        public AutoRefAttribute(AutoRefType refType,string name)
        {
            this.name = name;
            this.refType = refType;
        }

        public AutoRefType refType { get; set; }

        public string name { get; set; }
    }

    public enum AutoRefType
    {
        Self,
        Parent,
        Child,
        ChildName,
        None,
        Scene
    }
}
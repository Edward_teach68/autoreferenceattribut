﻿using System;
using UnityEngine;

namespace AutoReferencer
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoRefButtonAttribute : PropertyAttribute
    {

    }
}
﻿using AttributeConsole;
using AutoReferencer;
using UnityEngine;
// ReSharper disable All

[AutoRef(AutoRefType.Child)]
public class Test : MonoBehaviour
{
    public int X;

    public Account Account;
    
    [AutoRef("Account (1)")]
    public Account Pepega;
    
    [AutoRef(AutoRefType.Self)]
    public Customer Customer;
    
    [AutoRef("Account")]
    public Customer CustomerChild;
      
    [AutoRefButton]
    public bool button;
}

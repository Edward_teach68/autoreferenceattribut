﻿using System.Collections;
using System.Collections.Generic;
using Observer;
using UnityEngine;

public class TestObserver : MonoBehaviour
{    
    void Start()
    {
        Pepega pepega = new Pepega();
    }

    public void Logout()
    {
        LogOut.GetInstance().Logout();
    }
}

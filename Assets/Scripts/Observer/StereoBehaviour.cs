﻿namespace Observer
{
    public abstract class StereoBehaviour
    {
        protected StereoBehaviour() => LogOut.GetInstance().RegisterObserver(this);
        public abstract void OnLogout();
    }
}

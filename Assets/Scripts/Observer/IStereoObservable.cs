﻿using UnityEngine;

namespace Observer
{
    public interface IStereoObservable
    {
        void RegisterObserver(StereoBehaviour o);
        void RemoveObserver(StereoBehaviour o);
        void Logout();
    }
}

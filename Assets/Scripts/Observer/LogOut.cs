﻿using System.Collections;
using System.Collections.Generic;
using Observer;
using UnityEngine;

public class LogOut
{
    private static LogOut instance;
    List<StereoBehaviour> observers = new List<StereoBehaviour>();
    
    public static LogOut GetInstance() => instance ?? (instance = new LogOut());       
    public void RegisterObserver(StereoBehaviour o) => observers.Add(o);
    public void Logout() => observers.ForEach(ob => ob.OnLogout());
    
}
